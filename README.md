# Introduction

In this project, we will be building a product reviews API. Users will be able to add, view, update and delete products. Users will also be able to rate and review a product.

## Table of Contents
1. <a href="#technology-stack">Technology Stack</a>
2. <a href="#application-features">Application Features</a>
3. <a href="#api-endpoints">API Endpoints</a>
4. <a href="#setup">Setup</a>
5. <a href="#author">Author</a>
6. <a href="#license">License</a>

## Technology Stack
- [PHP](https://www.php.net)
- [Composer](https://getcomposer.org)
- [MySQL](https://www.mysql.com)
- [Laravel](https://laravel.com)

## Application Features
* User can sign up and sign in
* User can add and view products
* User can update or delete his product
* User can add reviews for a product
* User can update or delete his review

## API Endpoints
Method | Route | Description
--- | --- | ---
`POST` | `/api/register` | Create a user
`POST` | `/api/login` | Login an already registered user
`GET` | `/api/products` | View all products
`GET` | `/api/products/:id` | View a single product
`POST` | `/api/products/:productId/reviews` | Create a review for a product
`PUT` | `/api/products/:productId/reviews/:reviewId` | Update a product review
`DELETE` | `/api/products/:productId/reviews/:reviewId` | Delete a product review

### Create a user
```bash
curl -X POST -H 'Content-Type: application/json' -d '{
    "name": "John Doe",
    "email": "john.doe@example.com",
    "password": "Qwerty123$"
}' http://127.0.0.1:8000/api/register -i
```
### Login an already registered user
```bash
curl -X POST -H 'Content-Type: application/json' -d '{
    "email": "john.doe@example.com",
    "password": "Qwerty123$"
}' http://127.0.0.1:8000/api/login -i
```
### View all products
```bash
curl -X GET http://127.0.0.1:8000/api/products -i \
 -H "Accept: application/json" \
 -H "Content-Type: application/json"
```
### View a single product
```bash
curl -X GET http://127.0.0.1:8000/api/products/1 -i \
 -H "Accept: application/json" \
 -H "Content-Type: application/json"
```
### Create a review for a product
```bash
curl -X POST -H 'Content-Type: application/json' -d '{
    "review": "The best product",
    "rating": "5",
    "user_id": 1
}' http://127.0.0.1:8000/api/products/1/reviews -i
```
### Update a product review
```bash
curl -X PUT -H 'Content-Type: application/json' -d '{
    "review": "The good product",
    "rating": "4",
    "user_id": "2"
}' http://127.0.0.1:8000/api/products/4/reviews/20 -i
```
### Delete a product review
```bash
curl -X DELETE http://127.0.0.1:8000/api/products/4/reviews/20 -i \
 -H "Accept: application/json" \
 -H "Content-Type: application/json"
```

## Setup
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Dependencies
- [PHP](https://www.php.net)
- [Composer](https://getcomposer.org)
- [MySql](https://www.mysql.com)
- [Laravel](https://laravel.com)

### Getting Started
```bash
git clone https://github.com/Steelze/product-review-api.git
cd product-review-api
composer install
```
- Duplicate and save .env.example as .env and fill in environment variables
```bash
php artisan migrate
```
### Run The Service
```bash
php artisan serve
```

## Author
Odunayo Ileri Ogungbure

Tutorial from [dev.to](https://dev.to/mr_steelze/build-a-product-review-rest-api-with-laravel-2ih8).

### License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
