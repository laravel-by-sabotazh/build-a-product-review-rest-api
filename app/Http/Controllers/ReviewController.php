<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Review;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return JsonResponse
     */
    public function store(Request $request, Product $product): JsonResponse
    {
        $request->validate([
            'review' => 'required|string',
            'rating' => 'required|numeric|min:0|max:5',
        ]);

//        $review = new Review;
//        $review->review = $request->review;
//        $review->rating = $request->rating;
//        $review->user_id = Auth::id();

        $insert = [
            'review' => $request->get('review'),
            'rating' => $request->get('rating'),
            'user_id' => Auth::id() ?? $request->get('user_id')
        ];
        $review = new Review($insert);
        $product->reviews()->save($review);

        return response()->json(['message' => 'Review Added', 'review' => $review]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @param Review $review
     * @return JsonResponse
     */
    public function update(Request $request, Product $product, Review $review): JsonResponse
    {
//        if (Auth::id() !== $review->user_id) {
//            return response()->json(['message' => 'Action Forbidden']);
//        }
        $request->validate([
            'review' => 'required|string',
            'rating' => 'required|numeric|min:0|max:5',
        ]);

//        $review->review = $request->review;
//        $review->rating = $request->rating;
//        $review->save();

        $review->update($request->only('review', 'rating'));

        return response()->json(['message' => 'Review Updated', 'review' => $review]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @param Review $review
     * @return JsonResponse
     */
    public function destroy(Product $product, Review $review): JsonResponse
    {
//        if (Auth::id() !== $review->user_id) {
//            return response()->json(['message' => 'Action Forbidden']);
//        }
        $review->delete();

        return response()->json(null, 204);
    }
}
