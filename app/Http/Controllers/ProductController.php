<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $products = Product::with('user:id,name')
            ->withCount('reviews')
            ->get();
//            ->latest()
//            ->paginate(20);

        return response()->json(['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric|min:0',
        ]);

//        $product = new Product;
//        $product->name = $request->name;
//        $product->description = $request->description;
//        $product->price = $request->price;

        $insert = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
        ];
        $product = new Product($insert);

//        Auth::user()->products()->save($product);
//        auth()->user()->products()->save($product);
        $user = User::findOrFail($request->get('user_id'));
        $user->products()->save($product);

        return response()->json(['message' => 'Product Added', 'product' => $product]);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        $product->load(['reviews' => function ($query) {
            $query->latest();
        }, 'user']);

        return response()->json(['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return JsonResponse
     */
    public function update(Request $request, Product $product): JsonResponse
    {
//        if (Auth::id() !== $product->user_id) {
//            return response()->json(['message' => 'Action Forbidden']);
//        }
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
        ]);

//        $product->name = $request->name;
//        $product->description = $request->description;
//        $product->price = $request->price;
//        $product->save();

        $product->update($request->only('name', 'description', 'price'));

        return response()->json(['message' => 'Product Updated', 'product' => $product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function destroy(Product $product): JsonResponse
    {
        if (Auth::id() !== $product->user_id) {
            return response()->json(['message' => 'Action Forbidden']);
        }
        $product->delete();

        return response()->json(null, 204);
    }
}
